# James Pritchard & Glen Anderson
# Student IDs: 15177810 & 14159592
# Assignment 1
# 159.251


# imports
from tkinter.messagebox import askokcancel
from tkinter.messagebox import showinfo
from tkinter.scrolledtext import ScrolledText
from tkinter.filedialog import *
from tkinter.simpledialog import askstring
import time
import xml.etree.ElementTree as et
import zipfile

# create Tkinter workspace
main_window = Tk(className=" Text Editor")
text_window = ScrolledText(main_window, height=100, width=400)


# Clear the workspace
def new_command():
    text_window.delete('1.0', END)


# Open a file picker and load text files into the workspace
def open_command():
    file = askopenfilename(initialdir="/home", title="Select a file", filetypes=(("text files", "*.txt"), ("Open Documents", "*.odt")))
    if file is not None:
        # If an ODT document, then read the content.xml out of the ODT and read all text entries.
        if file[-4:] == ".odt":
            unzip = zipfile.ZipFile(file)
            tree = et.fromstring(unzip.read("content.xml"))
            buffer = u""
            buffer += xml_to_string(tree) + "\n"
            text_window.insert('1.0', buffer)
        else:
            # Open standard txt file
            with open(file) as f:
                contents = f.read()
                text_window.insert('1.0', contents)
                f.close()


# convert ODT xml into usable string to display. Further text features may need to be added if any text is missing
def xml_to_string(root):
    buffer = u""
    for node in root:
        tag = node.tag[node.tag.index('}') + 1:]
        if tag == 'p' or tag == 'h':
            if node.text is not None:
                buffer += str(node.text) + xml_to_string(node) + "\n"
            else:
                buffer += "\n"
        elif tag == 'a' or tag == 'span':
            if node.text is not None:
                buffer += str(node.text) + xml_to_string(node)
            else:
                buffer += "\n"
        elif tag == 'tab':
            buffer += "    " + xml_to_string(node)
        elif tag == 's':
            buffer += " " + xml_to_string(node)
        else:
            buffer += xml_to_string(node)
    return buffer


# Open the file picker and chose a location to save workspace to
def save_command():
    file = asksaveasfile(mode='w')
    if file is not None:
        data = text_window.get('1.0', END + ' -1c')
        file.write(data)
        file.close()


# Kill the process
def exit_command():
    if askokcancel("Quit", "Do you really want to quit?"):
        main_window.destroy()


# Search workspace for matching string as entered by user, highlight first match found.
def search_command():
    word = askstring("Search", "Enter word to search")
    length = len(str(word))
    pos = text_window.search(word, '1.0', stopindex=END)
    row, col = pos.split('.')
    endlen = int(col) + length
    end = row + '.' + str(endlen)

    text_window.tag_add("sel", pos, end)


# Small about box showing developers
def about_command():
    label = showinfo("About", "Written by James Pritchard and Glen Anderson ")


# Get time and date from OS and insert it into first line of workspace
def tnd_command():
    now = time.strftime("%c")
    text_window.insert('1.0', now + "\n")

# Create menu bar
menu_strip = Menu(main_window)
main_window.config(menu=menu_strip)
file_menu = Menu(menu_strip)

# Create file menu cascade containing file, new open, save and exit
menu_strip.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New", command=new_command)
file_menu.add_command(label="Open...", command=open_command)
file_menu.add_command(label="Save", command=save_command)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=exit_command)

# Create search menu cascade containing search
search_menu = Menu(menu_strip)
menu_strip.add_cascade(label="Search", menu=search_menu)
search_menu.add_command(label="Search", command=search_command)

# Create edit menu cascade containing cut, copy and paste, seperator and add time and date
edit_menu = Menu(menu_strip)
menu_strip.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Cut", accelerator="Ctrl+X", command=lambda: text_window.focus_get().event_generate('<<Cut>>'))
edit_menu.add_command(label="Copy", accelerator="Ctrl+C", command=lambda: text_window.focus_get().event_generate('<<Copy>>'))
edit_menu.add_command(label="Paste", accelerator="Ctrl+V", command=lambda: text_window.focus_get().event_generate('<<Paste>>'))
edit_menu.add_separator()
edit_menu.add_command(label="Add T&D", command=tnd_command)

# Create help menu cascade containing about
help_menu = Menu(menu_strip)
menu_strip.add_cascade(label="Help", menu=help_menu)
help_menu.add_command(label="About...", command=about_command)

# pack the workspace and enter the main loop
text_window.pack()
mainloop()
