159.251 - Assignment 1 
=======================
By Glen Anderson **14159592** and James Pritchard **15177810**

Glen Anderson highlight links: 	4d1b2e0 and 26bb89b

James Pritchard highlight links: 95ccd66 and f8e5d56

Instructions for use: Run **assignment1.py**


**Notes about .odt functionality:** No external libraries were used in this functionality, only packages included in the Python standard package were used.

**Note about the assignment:** Originally Glen was doing the assignment with another partner who did not contact Glen so Glen and James teamed up in the final week of the assignment. Glen had an outline of the programme already created and James had the programme created up to (but not including) adding odt support. We decided to go with James's version of the assignment as the framework. This version was edited and had odt format support implemented by Glen. This ensured we both put in about as much work to the project as each other.

As we didn't have very long working together we made the best use of Git we could in our limited time by tracking issues, assigning tasks that were still to be completed (as both of us had independently created close to identical programme outlines) and communicating over Git. Due to our unique situation we were not able to use Git from the first steps of the project however we made an effort to implement Git in all steps possible once we teamed up.

File Menu
===========

To create a new file use the File menu and select **New**

To save a file use the File menu and select **Save** and navigate to your desired location.

To Open a file use the File menu and select **Open** and navigate to the file you wish to load.

To Exit the programme select **Exit** from the file menu.

Search Menu
=============

To search for a string of text select **Search** from the Search menu and enter the text you wish to search for.

Edit Menu
==========

**Cut Copy** & **Paste** features are built into the text editor and can all be accessed from the edit menu.

Current System Time and Date can be added to the first line of your document by selecting **Add T&D** from the edit menu.

Help Menu
==========

To see the about window select **About** from the help menu.